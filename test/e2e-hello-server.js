const tls = require('tls')
const HServer = require('../src/hserver')
const ancryptoo = require('../crypto/cryptoption')

const sopt = {
  ancryptoo,
  port: 4444,
  tls,
  ALPNProtocols: ['h2', 'h2c']
}

const hcs = new HServer(sopt)

hcs.get('/', (req, res) => {
  res.setHeader({
    'content-type': 'text/html; charset=UTF-8'
  })
  res.send('hello world')
})

hcs.post('/', (req, res) => {
  res.setHeader({
    'content-type': 'text/html; charset=UTF-8'
  })
  res.send('hello world')
})
