const http2 = require('http2')
const ancryptoo = require('../crypto/cryptoption')

const time = (new Date()).getTime()

const counters = {
  error: 0,
  response: 0,
  close: 0,
  goaway: 0,
  frameError: 0
}

let stats = false

const logger = console

const responseCallback = (...rest) => {
  const [headers, flags] = rest
  if (!stats) {
    logger.log('responseCallback arguments', ...rest)
    logger.log('flags', flags)
    Object.keys(headers).forEach((key) => logger.log(`${key}: ${headers[key]}`))
  }
  counters.response += 1
}

function onResponse (req, cb) {
  req.on('response', cb)
}

const errorCallback = (err) => {
  if (!stats) logger.log('error is', err)
  counters.error += 1
}

function onError (req, cb) {
  req.on('error', cb)
}

const goawayCallback = (...rest) => {
  if (!stats) logger.log('goaway event fired with args', ...rest)
  counters.goaway += 1
}

function onGoAway (req, cb) {
  req.on('goaway', cb)
}

const closeCallback = (...rest) => {
  if (!stats) logger.log('close event fired with args', ...rest)
  counters.close += 1
}

function onClose (req, cb) {
  req.on('close', cb)
}

const frameErrorCallback = (ferr) => {
  if (!stats) logger.log('frameError is', ferr)
  counters.frameError += 1
}

function onFrameError (req, cb) {
  req.on('frameError', cb)
}

const dataCallbackWrap = (dataBuffer) => (chunk) => dataBuffer.push(chunk)

function onData (req, cb) {
  req.setEncoding('utf8')
  req.on('data', cb)
}

const endCallbackWrap = (dataBuffer, conncounter, client) => () => {
  if (!stats) logger.log('on end dataBuffer is :', dataBuffer.join(''))
  conncounter.pop()
  if (conncounter.length === 0) {
    client.close()
    counters.close += 1
    if (stats) logger.log('stats :', counters, 'time : ', (new Date()).getTime() - time)
  }
}

function onEnd (req, cb) {
  req.on('end', cb)
}

function doRequest (reqArray) {
  reqArray.forEach((ro) => {
    const client = http2.connect('https://server.hcore:4444', {
      ca: ancryptoo.ca
    })
    const finalro = {
      ':method': ro[':method'],
      ':path': ro[':path']
    }
    if (ro[':method'] === 'POST') {
      finalro['Content-Type'] = typeof ro.data === 'string' ? 'text/html' : 'application/json'
      finalro['Content-Length'] = ro.data.toString().length
    }
    const r = client.request(finalro)
    onError(r, errorCallback)
    onFrameError(r, frameErrorCallback)
    onGoAway(r, goawayCallback)
    onClose(r, closeCallback)
    onResponse(r, responseCallback)
    const dtBuffer = []
    const dataCallback = dataCallbackWrap(dtBuffer)
    onData(r, dataCallback)
    const endCallback = endCallbackWrap(dtBuffer, reqArray, client)
    onEnd(r, endCallback)
    if (ro[':method'] === 'POST') {
      r.write(ro.data.toString())
      r.end()
    }
  })
}

const times = process.argv.length > 2 ? parseInt(process.argv[2], 10) : 0
if (times) {
  if (process.argv[3] === 'stats') stats = true
  const reqs = []
  for (let t = 1; t <= times; t += 1) {
    if (t % 2 === 0) {
      reqs.push({ ':method': 'GET', ':path': '/' })
    } else {
      const data = t % 3 === 0 ? { msg: 'hello from client '.concat(t) } : 'hello from client '.concat(t)
      reqs.push({ ':method': 'POST', ':path': '/', data })
    }
  }
  doRequest(reqs)
}
